<?php
/**
* this tpl is just used to wrap comments
* there is no pre-processing for the box tpl
*/
$boxtitle = runvsc_build_tag_markup("h2", null, "title", $title) ;
$boxcontent = runvsc_build_tag_markup("div", null, "box-content", $content) ;
$classes[] = "box-content" ;
print runvsc_build_div_markup("box", $classes, $boxtitle . $boxcontent, true) ;