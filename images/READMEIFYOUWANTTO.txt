This is a placeholder so that I can get an empty the Images folder into CVS. But while I am here, let me tell you a joke:

Two developers are driving on a Highway. They switch on the radio and there is a warning: Please note that a car is driving on highway 75 against the traffic. The programmer near the driver looks at him and says: One? There are hundreds of them.

I pinched this from http://www.computerjokes.net/066.asp